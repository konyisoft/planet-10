# PLANET 10
**Prototype of an isometrically scrolling shooter game. Created with Unity3D 2019.2.**
****

![Planet 10 screenshot](Misc/screenshot.png)

[Try it out in browser](https://konyisoft.gitlab.io/planet-10/)
