﻿using UnityEngine;

namespace Konyisoft.Planet
{
	public static class Extensions
	{
		public static bool CompareAlphaOnly(this Color32 color, Color32 other)
		{
			// Only alpha checked
			return color.a == other.a;
		}

		public static bool CompareAlphaOnly(this Color32 color, float alpha)
		{
			// Only alpha checked
			return color.a == alpha;
		}
	}
}
