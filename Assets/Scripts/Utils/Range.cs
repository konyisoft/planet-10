using System;

namespace Konyisoft.Planet
{
	[Serializable]
	public class Range<T> where T : IComparable<T>
	{
		public T minimum;

		public T maximum;

		public override string ToString()
		{
			return string.Format("[{0} - {1}]", this.minimum, this.maximum);
		}

		public bool IsValid()
		{
			return this.minimum.CompareTo(this.maximum) <= 0;
		}

		public bool ContainsValue(T value)
		{
			return (this.minimum.CompareTo(value) <= 0) && (value.CompareTo(this.maximum) <= 0);
		}

		public bool IsInsideRange(Range<T> range)
		{
			return this.IsValid() && range.IsValid() && range.ContainsValue(this.minimum) && range.ContainsValue(this.maximum);
		}

		public bool ContainsRange(Range<T> range)
		{
			return this.IsValid() && range.IsValid() && this.ContainsValue(range.minimum) && this.ContainsValue(range.maximum);
		}
	}

	[Serializable]
	public class FloatRange : Range<float>
	{

	}
}