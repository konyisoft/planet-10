﻿namespace Konyisoft.Planet
{
	public static class MathUtils
	{
		public static float Normalize(float value, float min, float max)
		{
			return (value - min) / (max - min);
		}
	}
}
