using UnityEngine;
using UnityEditor;

namespace Konyisoft.Planet
{
	[CustomEditor(typeof(Comment))]
	public class CommentEditor : Editor
	{
		private float height = 0;
		private Comment comment;

		public override void OnInspectorGUI()
		{
			GUIStyle style = EditorGUIUtility.GetBuiltinSkin(EditorSkin.Inspector).GetStyle("TextArea");

			// Keep original values
			Color origColor = style.normal.textColor;
			RectOffset origPadding = style.padding;

			style.padding = new RectOffset(4, 4, 4, 4);
			if (EditorGUIUtility.isProSkin)
			{
				style.normal.textColor = new Color(244f / 255f, 188f / 255f, 2f / 255f);
			}
			else
			{
				style.normal.textColor = new Color(60f / 255f, 60f / 255f, 60f / 255f);
			}

			// Min height
			if (height < 50)
				height = 50;

			comment.text = EditorGUILayout.TextArea(comment.text, style, GUILayout.Height(height));

			// Calculate content height
			Rect lastRect = GUILayoutUtility.GetLastRect();
			if (lastRect.width > 1.0 && lastRect.height > 1.0)
			{
				string text = comment.text;
				height = style.CalcHeight(new GUIContent(text), lastRect.width);
			}

			// Restore original values
			style.normal.textColor = origColor;
			style.padding = origPadding;
		}

		void OnEnable()
		{
			comment = target as Comment;
		}
	}
}

