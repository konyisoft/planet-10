using UnityEngine;
using System.Collections;

namespace Konyisoft.Planet
{
	public class Player : GameBehaviour
	{
		#region Fields

		[Header("Object")]
		public GameObject playerMeshObject;
		public Transform bulletSpawn;
		public Transform explosionSpawn;

		[Header("Movement")]
		public float flyingSpeed = 1f;
		public float horizontalSpeed = 1f;
		public FloatRange horizontalConstrains = new FloatRange();
		public float verticalSpeed = 1f;
		public FloatRange verticalConstrains = new FloatRange();
		public float rollDamping = 10f;
		public float maxRollAngle = 20f;

		[Header("Item related")]
		public float maxFuel = 100f;
		public float fuelConsumption = 1f; // per second
		public float consumptionMultiplier = 2f;
		public float speedModifierDuration = 3f;
		public float speedModifierMultiplier = 2f;

		[Header("Shooting")]
		public float shootInterval = 0.25f;

		[Header("(In)vulnerability")]
		public int blinkCount = 10;
		public float blinkInerval = 0.5f;

		Vector3 startPosition;
		Vector3 previousPosition;
		Vector3 origEulerAngles;
		float currentSpeed;
		float fuel;
		float traveledDistance;
		float horizontalMoveDelta;
		float verticalMoveDelta;
		float roll;
		float speedModifierTimer;
		bool isMaximizingFuel;
		bool isShooting;
		float shootTimer;

		#endregion

		#region Properties

		public bool IsFlying
		{
			get; private set;
		}

		public bool IsDead
		{
			get; private set;
		}

		public bool IsVulnerable
		{
			get; private set;
		}

		public bool IsCheating
		{
			get; private set;
		}

		public float Fuel
		{
			get { return fuel; }
		}

		public float FuelNormalized
		{
			get { return MathUtils.Normalize(fuel, 0, maxFuel); }
		}

		public float TraveledDistance
		{
			get { return traveledDistance; }
		}

		public Vector3 Velocity
		{
			get { return transform.forward * currentSpeed; }
		}

		public Vector3 StartPosition
		{
			get { return startPosition; }
		}

		#endregion

		#region Mono methods

		void Awake()
		{
			origEulerAngles = transform.localEulerAngles;
			startPosition = transform.localPosition;

			if (bulletSpawn == null)
				bulletSpawn = transform;

			if (explosionSpawn == null)
				explosionSpawn = transform;

			Initialize();
		}

		void Update()
		{
			float deltaTime = Time.deltaTime; // shortcut

			// Fuel
			if (isMaximizingFuel)
			{
				fuel = Mathf.Lerp(fuel, maxFuel, deltaTime * 10f);
				if (Mathf.Abs(maxFuel - fuel) < 0.05f)
					isMaximizingFuel = false;
			}

			// Flying
			if (IsFlying)
			{
				// Counting down timer for modified speed
				if (speedModifierTimer > 0)
				{
					speedModifierTimer -= deltaTime;
				}
				// Timeout: back to normal speed
				else
				{
					speedModifierTimer = 0;
					currentSpeed = flyingSpeed;
				}

				// Count traveled distance
				traveledDistance += currentSpeed * deltaTime;

				// Moving
				Vector3 translation = new Vector3(horizontalMoveDelta * horizontalSpeed, 0, verticalMoveDelta * verticalSpeed);
				transform.Translate(translation * deltaTime, Space.World);

				// Position constraints
				Vector3 position = transform.localPosition;
				position.x = Mathf.Clamp(position.x, horizontalConstrains.minimum, horizontalConstrains.maximum);
				position.z = Mathf.Clamp(position.z, verticalConstrains.minimum, verticalConstrains.maximum);
				transform.localPosition = position;

				// Fuel (no consumption during maximizing)
				if (!isMaximizingFuel && !IsCheating)
				{
					// Consumption is faster when moving
					float multiplier = horizontalMoveDelta != 0 || verticalMoveDelta != 0 ? consumptionMultiplier : 1f;
					fuel -= fuelConsumption * deltaTime * multiplier;
					if (fuel <= 0)
					{
						fuel = 0;
						GameManager.Instance.OutOfFuel();
					}
				}
				previousPosition = transform.localPosition;
			}

			// Roll (rotation on Z axis)
			roll = Mathf.Lerp(roll, horizontalMoveDelta, rollDamping * deltaTime);
			transform.localEulerAngles = origEulerAngles + new Vector3(0, 0, -maxRollAngle * roll);

			// Shooting
			if (isShooting)
			{
				shootTimer += deltaTime;
				if (shootTimer >= shootInterval) {
					shootTimer = 0;
					CreateBullet();
				}
			}
		}

		#endregion

		#region Public methods

		public void Initialize()
		{
			StopAllCoroutines();

			transform.localPosition = startPosition;
			transform.localEulerAngles = origEulerAngles;
			previousPosition = startPosition;

			currentSpeed = 0;
			fuel = maxFuel;
			traveledDistance = 0;
			horizontalMoveDelta = 0;
			verticalMoveDelta = 0;
			roll = 0;
			speedModifierTimer = 0;
			isMaximizingFuel = false;
			isShooting = false;
			shootTimer = 0;

			IsFlying = false;
			IsDead = false;

			BeVulnerable();
		}

		public void Fly()
		{
			currentSpeed = flyingSpeed;
			horizontalMoveDelta = 0;
			verticalMoveDelta = 0;
			speedModifierTimer = 0;
			isShooting = false;
			shootTimer = 0;
			IsFlying = true;
		}

		public void Stop()
		{
			currentSpeed = 0;
			horizontalMoveDelta = 0;
			verticalMoveDelta = 0;
			speedModifierTimer = 0;
			isShooting = false;
			shootTimer = 0;
			IsFlying = false;
		}

		public void Move(Vector2 moveDelta)
		{
			horizontalMoveDelta = moveDelta.x;
			verticalMoveDelta = moveDelta.y;
		}

		public void Die()
		{
			Stop();
			Deactivate();
			CreateExplosion();
			IsDead = true;
		}

		public void Revive()
		{
			Activate();
			MaximizeFuel();
			Fly();
			BeInvulnerable();
			IsDead = false;
		}

		public void MaximizeFuel()
		{
			isMaximizingFuel = true;
		}

		public void SpeedUp()
		{
			speedModifierTimer = speedModifierDuration;
			currentSpeed = flyingSpeed * speedModifierMultiplier;
		}

		public void SlowDown()
		{
			speedModifierTimer = speedModifierDuration;
			currentSpeed = flyingSpeed / speedModifierMultiplier;
		}

		public void Shoot() {
			isShooting = true;
			shootTimer = 0;
			CreateBullet();  // one shot immediately
		}

		public void DontShoot() {
			isShooting = false;
		}

		public void BeVulnerable()
		{
			IsVulnerable = true;
		}

		public void BeInvulnerable()
		{
			IsVulnerable = false;
			StartCoroutine(CO_BeInvulnerable());
		}

		public void EnableCheat()
		{
			StopAllCoroutines();
			MaximizeFuel();
			playerMeshObject.SetActive(true);
			IsVulnerable = false;
			IsCheating = true;
		}

		public void DisableCheat()
		{
			IsVulnerable = true;
			IsCheating = false;
		}

		#endregion

		#region Private methods

		void Activate() {
			gameObject.SetActive(true);
		}

		void Deactivate() {
			gameObject.SetActive(false);
		}

		void CreateBullet()
		{
			if (PlayerManager.Instance.bulletPrefab != null)
			{
				PooledObject pooledObject = PlayerManager.Instance.bulletPrefab.GetPooledInstance<PooledObject>();
				pooledObject.transform.SetParent(GameManager.Instance.temporaryObject); // put below temp object
				(pooledObject as Bullet).Shot(bulletSpawn);
			}
		}

		void CreateExplosion()
		{
			if (PlayerManager.Instance.explosionPrefab != null)
			{
				PooledObject pooledObject = PlayerManager.Instance.explosionPrefab.GetPooledInstance<PooledObject>();
				pooledObject.transform.SetParent(GameManager.Instance.temporaryObject); // put below temp object
				(pooledObject as Explosion).Explode(explosionSpawn);
			}
		}

		IEnumerator CO_BeInvulnerable()
		{
			if (playerMeshObject != null)
			{
				// Blinks player with simply activating/deactivating its gameObject
				int counter = 0;
				bool active = false;
				while (counter < blinkCount)
				{
					playerMeshObject.SetActive(active);
					yield return new WaitForSeconds(blinkInerval);
					counter++;
					active = !active;
				}
				playerMeshObject.SetActive(true);
			}
			else
			{
				yield return new WaitForSeconds(blinkInerval * blinkCount);
			}

			if (!IsCheating)
				BeVulnerable();

			yield return null;
		}

		#endregion

		#region Event handlers

		void OnTriggerEnter(Collider other)
		{
			if (IsVulnerable && other.gameObject.CompareTag(Constants.Tags.EnemyBullet))
			{
				// Return bullet to pool
				PoolManager.Instance.ReturnToPool(other);
				GameManager.Instance.Impact();
			}
		}

		#endregion
	}
}
