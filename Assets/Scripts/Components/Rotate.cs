using UnityEngine;

namespace Konyisoft.Planet
{
	public class Rotate : GameBehaviour
	{
		#region Fields
		
		public Vector3 rotation;
		public Space space = Space.Self;
		
		#endregion
		
		#region Mono methods
		
		void Update()
		{
			transform.Rotate(rotation * Time.deltaTime, space);
		}
		
		#endregion
	}
}
