using UnityEngine;

namespace Konyisoft.Planet
{
	public class Bullet : PooledObject
	{
		#region Fields

		public float speed = 7.5f;
		public float destroyDistance = 5f;
		public bool applyLevelMovement;

		Vector3 origPosition;
		Quaternion origRotation;

		#endregion

		#region Properties

		public bool IsOperating
		{
			get; private set;
		}

		#endregion

		#region Mono methods

		void Update()
		{
			if (!IsOperating)
				return;

			float sqrDistance = Vector3.SqrMagnitude(origPosition - transform.position);
			// Out of max distance
			if (sqrDistance > destroyDistance * destroyDistance)
			{
				IsOperating = false;
				ReturnToPool();
			// Move to direction/speed
			} else {
				Vector3 position = transform.position;
				position += transform.forward * speed * Time.deltaTime;
				if (applyLevelMovement)
					position += -PlayerManager.Instance.Player.Velocity * Time.deltaTime;
				// Lerp to players y position
				position.y = Mathf.Lerp(position.y, PlayerManager.Instance.Player.transform.position.y, speed * Time.deltaTime);
				transform.position = position;
			}
		}

		#endregion

		#region Public methods

		public void Shot(Transform spwan)
		{
			transform.position = origPosition = spwan.position;
			transform.rotation = origRotation = spwan.rotation;
			IsOperating = true;
		}

		#endregion
	}
}

