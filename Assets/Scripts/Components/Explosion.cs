using UnityEngine;

namespace Konyisoft.Planet
{
	[RequireComponent(typeof(ParticleSystem))]
	public class Explosion : PooledObject
	{
		#region Fields

		ParticleSystem ps;

		#endregion

		#region Mono methods

		void Awake()
		{
			ps = GetComponent<ParticleSystem>();
		}

		#endregion

		#region Protected methods

		protected override void OnDeactivate()
		{
			base.OnDeactivate();
			if (ps != null)
				ps.Clear();
		}

		#endregion

		#region Public methods

		public void Explode(Transform spawn)
		{
			transform.position = spawn.position;
			if (ps != null)
				ps.Play();
		}

		#endregion

		#region Event handlers

		void OnParticleSystemStopped()
		{
			ReturnToPool();
		}

		#endregion
	}
}
