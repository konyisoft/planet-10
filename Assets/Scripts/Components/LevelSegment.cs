using System.Collections.Generic;
using UnityEngine;

namespace Konyisoft.Planet
{
	public class LevelSegment : GameBehaviour
	{
		#region Fields

		List<PooledObject> pooledObjects = new List<PooledObject>();

		#endregion

		#region Public methods

		public void Create(int segmentIndex, int colCount, int rowCount, LevelSegment alignTo = null)
		{
			Clear();
			if (segmentIndex < LevelManager.Instance.SegmentCount)
			{
				// NOTE: pixels' alpha channel can be used to store additional information (byte, 0-255)

				// Shortcut
				Color32[] objectPixels = LevelManager.Instance.ObjectPixels;
				Color32[] groundPixels = LevelManager.Instance.GroundPixels;

				// Column counter
				int col = 0;

				// Coords of the object instance
				float x = 0;
				float z = 0;

				// Calculate indexes in pixels array
				int startIndex = objectPixels.Length - (segmentIndex * rowCount * colCount);
				int endIndex = startIndex - (rowCount * colCount);
				if (endIndex < 0)
					endIndex = 0; // Avoid 'index out of bounds'

				float objectSize = LevelManager.Instance.objectSize;

				// Going backward in pixels array
				for (int i = startIndex - 1; i >= endIndex; i--)
				{
					Color32 objectPixel = objectPixels[i];
					// Object pixel: alpha is not 0?
					if (!objectPixel.CompareAlphaOnly(0))
					{
						// Find and instantiate the prefab assigned to current pixel color
						PooledObject prefab = LevelManager.Instance.GetObjectPrefab(objectPixel);
						if (prefab != null)
						{
							PooledObject pooledObject = prefab.GetPooledInstance<PooledObject>(objectPixel.a);
							// Adds global position offset + the object's position offset
							pooledObject.transform.position = new Vector3(x, 0, z) + LevelManager.Instance.objectOffset + pooledObject.positionOffset;
							pooledObject.transform.SetParent(transform, false);
							pooledObjects.Add(pooledObject);
						}
					}

					Color32 groundPixel = groundPixels[i];
					// Ground pixel: alpha is not 0?
					if (!groundPixel.CompareAlphaOnly(0))
					{
						// Find and instantiate the prefab assigned to current pixel color
						PooledObject prefab = LevelManager.Instance.GetGroundPrefab(groundPixel);
						if (prefab != null)
						{
							PooledObject pooledObject = prefab.GetPooledInstance<PooledObject>(groundPixel.a);
							pooledObject.transform.position = new Vector3(x, 0, z) + LevelManager.Instance.groundOffset + pooledObject.positionOffset;
							pooledObject.transform.SetParent(transform, false);
							pooledObjects.Add(pooledObject);
						}
					}

					// Update coords
					col++;
					x += objectSize;

					if (col == colCount)
					{
						col = 0;
						x = 0;
						z += objectSize;
					}
				}

				Align(alignTo, new Vector3(0, 0, rowCount * objectSize));
			}
		}

		public void Clear()
		{
			for (int i = 0; i < pooledObjects.Count; i++)
			{
				pooledObjects[i].ReturnToPool();
			}
			pooledObjects.Clear();
		}

		public void DetachPooledObject(PooledObject pooledObject, Transform newParent = null)
		{
			pooledObjects.Remove(pooledObject);
			pooledObject.transform.SetParent(newParent, true);
		}

		#endregion

		#region Private methods

		void Align(LevelSegment alignTo, Vector3 translation)
		{
			if (alignTo != null)
			{
				transform.SetParent(alignTo.transform, false);
				transform.localPosition = Vector3.zero;
				transform.Translate(translation);
				transform.SetParent(alignTo.transform.parent);
				transform.localScale = Vector3.one;
			}
		}

		#endregion
	}
}
