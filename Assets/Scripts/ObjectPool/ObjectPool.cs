using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Konyisoft.Planet
{
	public class ObjectPool : GameBehaviour
	{
		#region Fields

		PooledObject prefab;
		List<PooledObject> unused = new List<PooledObject>();

		const float ObjectLifeTime = 15f;
		const float CleanupInterval = 15f;
		const string CleanupMethod = "Cleanup";

		#endregion

		#region Mono methods

		void Start()
		{
			InvokeRepeating(CleanupMethod, CleanupInterval, CleanupInterval);
		}

		#endregion

		#region Private methods

		void Cleanup()
		{
			unused.ForEach(pooledObject =>
			{
				if (Time.timeSinceLevelLoad - pooledObject.DeactivationTime > ObjectLifeTime)
				{
					DestroyImmediate(pooledObject.gameObject);
					pooledObject = null;
				}
			});
			unused.RemoveAll(pooledObject => pooledObject == null);
		}

		#endregion

		#region Public methods

		public PooledObject GetObject()
		{
			PooledObject pooledObject;
			int lastAvailableIndex = unused.Count - 1;
			if (lastAvailableIndex >= 0)
			{
				pooledObject = unused[lastAvailableIndex];
				unused.RemoveAt(lastAvailableIndex);
				pooledObject.gameObject.SetActive(true);
			}
			else
			{
				pooledObject = Instantiate<PooledObject>(prefab);
				pooledObject.transform.SetParent(transform, false);
				pooledObject.Pool = this;
			}
			return pooledObject;
		}

		public void AddObject(PooledObject pooledObject)
		{
			pooledObject.gameObject.SetActive(false);
			pooledObject.transform.SetParent(transform, false);
			unused.Add(pooledObject);
		}

		public static ObjectPool GetPool(PooledObject prefab)
		{
			GameObject go;
			ObjectPool pool;
			if (Application.isEditor)
			{
				go = GameObject.Find(prefab.name + " Pool");
				if (go != null)
				{
					pool = go.GetComponent<ObjectPool>();
					if (pool != null)
					{
						return pool;
					}
				}
			}
			go = new GameObject(prefab.name + " Pool");
			pool = go.AddComponent<ObjectPool>();
			pool.prefab = prefab;
			pool.transform.SetParent(PoolManager.Instance.poolsParent);
			return pool;
		}

		#endregion
	}
}
