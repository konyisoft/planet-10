using System;
using UnityEngine;

namespace Konyisoft.Planet
{
	public class PooledObject : GameBehaviour
	{
		#region Fields

		public Vector3 positionOffset;

		[NonSerialized]
		ObjectPool poolInstanceForPrefab;
		float deactivationTime;

		#endregion

		#region Properties

		public ObjectPool Pool
		{
			get; set;
		}

		public float DeactivationTime
		{
			get { return deactivationTime; }
		}

		public byte AdditionalData
		{
			get; private set;
		}

		#endregion

		#region Public methods

		public void ReturnToPool()
		{
			if (Pool != null)
			{
				Pool.AddObject(this);
				deactivationTime = Time.timeSinceLevelLoad;
				this.OnDeactivate();
			}
			else
			{
				Destroy(gameObject);
			}
		}

		public T GetPooledInstance<T>(byte value = 255) where T : PooledObject
		{
			if (poolInstanceForPrefab == null)
			{
				poolInstanceForPrefab = ObjectPool.GetPool(this);
			}
			T instance = (T)poolInstanceForPrefab.GetObject();
			instance.AdditionalData = value; // sets additional data property
			instance.OnActivate();
			return instance;
		}

		#endregion

		#region Protected methods

		protected virtual void OnActivate()
		{
		}

		protected virtual void OnDeactivate()
		{
		}

		#endregion
	}
}
