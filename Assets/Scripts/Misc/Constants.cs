﻿using System.Collections.Generic;

namespace Konyisoft.Planet
{
	public static class Constants
	{
		public static class Tags
		{
			public const string Player = "Player";
			public const string PlayerBullet = "PlayerBullet";
			public const string EnemyBullet = "EnemyBullet";
		}

		public static class Layers
		{
			public const int Default = 0;
			public const int Unblocked = 9;
		}

		public static class LayerMasks
		{
			public const int Default = 1 << Layers.Default;
			public const int Unblocked = 1 << Layers.Unblocked;
		}

		public static class Texts
		{
			public const string Score = "SCORE <color=#ffff44>{0:D7}</color>";
			public const string Lives = "LIVES <color=#ffff44>{0}</color>";

			public static class Message
			{
				const string HitEnterToRestart = "HIT [ENTER] TO RESTART";
				const string HitEnterToContinue = "HIT [ENTER] TO CONTINUE";

				public const string Failed = "FAILED\n" + HitEnterToContinue;
				public const string OutOfFuel = "OUT OF FUEL\n" + HitEnterToContinue;
				public const string MaximizeFuel = "FUEL ON MAXIMUM";
				public const string GainOneLife = "+1 LIFE";
				public const string LivesOnMaximum = "LIVES ON MAXIMUM";
				public const string Bonus = "BONUS +{0}";
				public const string SpeedUp = "SPEED UP";
				public const string SlowDown = "SLOW DOWN";
				public const string GameOver = "GAME OVER\n" + HitEnterToRestart;
				public const string Won = "THE END\n" + HitEnterToRestart;
			}
		}

		public static class ResourceData {

			static string DirPrefabs = "Prefabs/";
			static string DirAdditional = DirPrefabs + "Additional/";
			static string DirEnemies = DirPrefabs + "Enemies/";
			static string DirGrounds = DirPrefabs + "Grounds/";
			static string DirItems = DirPrefabs + "Items/";
			static string DirObstacles = DirPrefabs + "Obstacles/";

			public static Dictionary<string, string> Objects = new Dictionary<string, string>()
			{
				// Items, common
				{ "#0000ff", DirItems + "Fuel" },
				{ "#00ff00", DirItems + "Speed Up" },
				{ "#ff0000", DirItems + "Slow Down" },
				{ "#ffff00", DirItems + "Bonus" },
				{ "#fafafa", DirItems + "Life" },

				// Enemies
				{ "#333333", DirEnemies + "Enemy Drone Black" },
				{ "#222222", DirEnemies + "Enemy Ship Black" },
				{ "#cb1b45", DirEnemies + "Enemy Ship Red" },
				{ "#5c5c5c", DirEnemies + "Enemy Turret Grey" },
				{ "#6c6c6c", DirEnemies + "Enemy Cannon Grey" },

				// Desert
				{ "#ffd988", DirObstacles + "Desert Pyramid" },
				{ "#ffd06c", DirObstacles + "Desert Building" },
				{ "#fff3d8", DirObstacles + "Desert Wall" },
				{ "#ffedc6", DirAdditional + "Desert Rocks" },
				{ "#b2cc12", DirAdditional + "Desert Plants" },
				{ "#f59835", DirAdditional + "Desert Craters" },

				//Forest
				{ "#7ba23f", DirAdditional + "Forest Bushes" },
				{ "#1b813e", DirAdditional + "Forest Trees" },
			};

			public static Dictionary<string, string> Grounds = new Dictionary<string, string>()
			{
				// Desert
				{ "#ffe5af", DirGrounds + "Desert Grounds" },

				// Forest
				{ "#86c166", DirGrounds + "Forest Grounds" },

				// Water
				{ "#005caf", DirGrounds + "Water Grounds" },
				{ "#1e88a8", DirGrounds + "Water Grounds Light" },
			};

		}
	}
}
