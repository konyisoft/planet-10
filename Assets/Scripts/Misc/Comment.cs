using UnityEngine;

namespace Konyisoft.Planet
{
	public class Comment : MonoBehaviour
	{
		public string text = "Type your comment here...";

		void OnDrawGizmos()
		{
			Gizmos.DrawIcon(transform.position, "Comment");
		}
	}
}