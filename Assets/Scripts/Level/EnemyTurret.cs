using UnityEngine;

namespace Konyisoft.Planet
{
	public class EnemyTurret : EnemyObject
	{
		#region Field

		[Header("Turret")]
		public float rotateSpeed = 10f;

		#endregion

		#region Protected methods

		protected override void OnOperating()
		{
			base.OnOperating();
    		// Aims in front of the player (with 1 unit forward)
			Vector3 lookDirection = (PlayerManager.Instance.Player.transform.position + Vector3.forward) - transform.position;
    		lookDirection.y = 0;
    		Quaternion lookRotation = Quaternion.LookRotation(lookDirection);
			transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, rotateSpeed * Time.deltaTime);
		}

		#endregion
	}
}
