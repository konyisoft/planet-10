namespace Konyisoft.Planet
{
	public class SpeedUp : LevelObject
	{
		#region Protected methods

		protected override void OnCollideWithPlayer()
		{
			GameManager.Instance.SpeedUp();
		}

		#endregion
	}
}
