namespace Konyisoft.Planet
{
	public class SimpleObstacle : LevelObject
	{
		#region Protected methods

		protected override void OnCollideWithPlayer()
		{
			if (PlayerManager.Instance.Player.IsVulnerable)
				GameManager.Instance.Impact();
		}

		#endregion
	}
}
