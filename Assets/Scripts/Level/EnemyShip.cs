using UnityEngine;

namespace Konyisoft.Planet
{
	public class EnemyShip : EnemyObject
	{
		#region Enums

		enum Movement
		{
			Straight,     // z axis only, default
			LeftToRight,  // x, z
			RightToLeft   // x, z
		}

		#endregion

		#region Fields

		[Header("Ship")]
		public float speedX = 1;
		public float speedZ = 1;
		public float rollDamping = 10f;
		public float maxRollAngle = 20f;

		Vector3 origEulerAngles;
		float roll;

		Movement movement;

		#endregion

		#region Mono methods

		protected override void Awake()
		{
			base.Awake();
			origEulerAngles = transform.localEulerAngles;
		}

		#endregion

		#region Protected methods

		protected override void OnOperating()
		{
			base.OnOperating();
			float deltaTime = Time.deltaTime;
			switch (movement)
			{
				case Movement.Straight:
					transform.Translate(0, 0, -speedZ * deltaTime, Space.World);
					break;
				case Movement.LeftToRight:
					transform.Translate(speedX * deltaTime, 0, -speedZ * deltaTime, Space.World);
					roll = Mathf.Lerp(roll, 1f, rollDamping * deltaTime);
					transform.localEulerAngles = origEulerAngles + new Vector3(0, 0, maxRollAngle * roll);
					break;
				case Movement.RightToLeft:
					transform.Translate(-speedX * deltaTime, 0, -speedZ * deltaTime, Space.World);
					roll = Mathf.Lerp(roll, 1f, rollDamping * deltaTime);
					transform.localEulerAngles = origEulerAngles + new Vector3(0, 0, -maxRollAngle * roll);
					break;
			}
		}

		protected override void OnActivate()
		{
			base.OnActivate();
			// Different movement types according to additional data value
			// NOTE: due to some GIMP bugs(?), during image editing, you should use different grey values than the real ones for alpha channel.
			switch (AdditionalData)
			{
				case 255:
					movement = Movement.Straight;
					break;
				case 224:  // 241 in gimp
					movement = Movement.LeftToRight;
					break;
				case 192:  // 225
					movement = Movement.RightToLeft;
					break;
			}
			roll = 0;
			transform.localEulerAngles = origEulerAngles;

		}

		#endregion
	}
}
