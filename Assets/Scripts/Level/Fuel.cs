namespace Konyisoft.Planet
{
	public class Fuel : LevelObject
	{
		#region Protected methods

		protected override void OnCollideWithPlayer()
		{
			GameManager.Instance.MaximizeFuel();
		}

		#endregion
	}
}
