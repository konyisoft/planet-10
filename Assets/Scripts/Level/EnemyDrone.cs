using UnityEngine;

namespace Konyisoft.Planet
{
	public class EnemyDrone : EnemyObject
	{
		#region Fields

		[Header("Drone")]
		public float speed = 1f;
		public float rotateSpeed = 1f;
		public FloatRange horizontalConstraints;

		float direction = 1;

		#endregion

		#region Protected methods

		protected override void OnOperating()
		{
			base.OnOperating();
			transform.Rotate(0, rotateSpeed, 0);
			float x = transform.position.x;
			if (x <= horizontalConstraints.minimum)
			{
				direction = 1;
			}
			else if (x >= horizontalConstraints.maximum)
			{
				direction = -1;
			}
			transform.Translate(direction * speed * Time.deltaTime, 0, 0, Space.World);
		}

		#endregion
	}
}
