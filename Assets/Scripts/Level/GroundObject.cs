namespace Konyisoft.Planet
{
	// Base class for all ground objects. They can not interact with the player.
	public class GroundObject : VariableObject
	{
		// Does nothing at this time
	}
}
