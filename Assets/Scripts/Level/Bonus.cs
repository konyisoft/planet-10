namespace Konyisoft.Planet
{
	public class Bonus : LevelObject
	{
		#region Fields

		public int score = 1000;

		#endregion

		#region Protected methods

		protected override void OnCollideWithPlayer()
		{
			GameManager.Instance.Bonus(score);
		}

		#endregion
	}
}
