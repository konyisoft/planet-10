using UnityEngine;

namespace Konyisoft.Planet
{
	// Base class for all enemies
	public class EnemyObject : LevelObject
	{
		#region Enums

		public enum EnemyState
		{
			Waiting,
			Operating,
			Terminated
		}

		public enum OperatingMode
		{
			DetachFromLevel,
			StayOnLevel
		}

		#endregion

		#region Fields

		[Header("Common")]
		public Bullet bulletPrefab;
		public Transform bulletSpawn;
		public Explosion explosionPrefab;
		public Transform explosionSpawn;
		public OperatingMode operatingMode;
		public float operatingDistance;
		public float terminateDistance;
		public bool canCollideWithPlayer = true;
		public bool canBeShot = true;
		public bool canShoot = true;
		public float shootDelay = 0.25f;
		public float shootInterval = 0.25f;
		public int score = 100;

		float shootTimer;

		#endregion

		#region Properties

		public EnemyState State
		{
			get; private set;
		}

		public bool IsShooting
		{
			get; private set;
		}

		#endregion

		#region Mono methods

		protected override void Awake()
		{
			base.Awake();

			// Set defaults from EnemyManager
			if (bulletPrefab == null)
				bulletPrefab = EnemyManager.Instance.bulletPrefab;

			if (explosionPrefab == null)
				explosionPrefab = EnemyManager.Instance.explosionPrefab;

			if (operatingDistance == 0)
				operatingDistance = EnemyManager.Instance.operatingDistance;

			if (terminateDistance == 0)
				terminateDistance = EnemyManager.Instance.terminateDistance;

			if (bulletSpawn == null)
				bulletSpawn = transform;

			if (explosionSpawn == null)
				explosionSpawn = transform;
		}

		protected virtual void Update()
		{
			if (IsState(EnemyState.Terminated))
				return;

			// Distance between enemy and player's start point (measured only on Z axis)
			float distance = (transform.position - PlayerManager.Instance.Player.StartPosition).z;
			// Start to operate
			if (IsState(EnemyState.Waiting) && distance <= operatingDistance)
			{
				OnOperationStart();
			}
			// Oprating/shooting or terminate
			else if (IsState(EnemyState.Operating))
			{
				if (distance <= terminateDistance)
				{
					OnOperationEnd();
				}
				else
				{
					OnOperating();

					// Shooting
					if (canShoot)
					{
						shootTimer += Time.deltaTime;
						// Delay at start
						if (!IsShooting && shootTimer >= shootDelay)
						{
							IsShooting = true;
						}
						// Shoot
						if (IsShooting && shootTimer >= shootInterval) {
							shootTimer = 0;
							CreateBullet();
							OnShoot();
						}
					}
				}
			}
		}

		#endregion

		#region Protected methods

		protected override void OnCollideWithPlayer()
		{
			if (canCollideWithPlayer && PlayerManager.Instance.Player.IsVulnerable)
				GameManager.Instance.Impact();
		}

		protected override void OnTriggerEnter(Collider other)
		{
			base.OnTriggerEnter(other);
			if (IsState(EnemyState.Operating) && canBeShot && other.gameObject.CompareTag(Constants.Tags.PlayerBullet))
			{
				// Return bullet to pool
				PoolManager.Instance.ReturnToPool(other);
				OnCollideWithPlayerBullet();
			}
		}

		protected override void OnActivate()
		{
			shootTimer = 0;
			SetState(EnemyState.Waiting);
		}

		protected virtual void OnOperationStart()
		{
			SetState(EnemyState.Operating);
			if (IsOperatingMode(OperatingMode.DetachFromLevel))
				ParentSegment.DetachPooledObject(this, EnemyManager.Instance.enemiesParent);
		}

		protected virtual void OnOperating()
		{
		}

		protected virtual void OnOperationEnd()
		{
			SetState(EnemyState.Terminated);
			if (IsOperatingMode(OperatingMode.DetachFromLevel))
			{
				ReturnToPool();
			}
			// NOTE: Nothing to do with StayOnLevel objects here but wait for the segment to be cleared
		}

		protected virtual void OnShoot()
		{
		}

		protected virtual void OnCollideWithPlayerBullet()
		{
			SetState(EnemyState.Terminated);
			CreateExplosion();
			GameManager.Instance.IncreaseScore(score);
			if (IsOperatingMode(OperatingMode.DetachFromLevel))
			{
				ReturnToPool();
			}
			else
			{
				gameObject.SetActive(false);
			}
		}

		#endregion

		#region Private methods

		void SetState(EnemyState state)
		{
			State = state;
		}

		bool IsState(EnemyState state)
		{
			return State == state;
		}

		bool IsOperatingMode(OperatingMode mode)
		{
			return operatingMode == mode;
		}

		void CreateBullet()
		{
			if (bulletPrefab != null)
			{
				PooledObject pooledObject = bulletPrefab.GetPooledInstance<PooledObject>();
				pooledObject.transform.SetParent(GameManager.Instance.temporaryObject); // put below temp container
				(pooledObject as Bullet).Shot(bulletSpawn);
			}
		}

		void CreateExplosion()
		{
			if (explosionPrefab != null)
			{
				PooledObject pooledObject = explosionPrefab.GetPooledInstance<PooledObject>();
				pooledObject.transform.SetParent(GameManager.Instance.temporaryObject); // put below temp container
				(pooledObject as Explosion).Explode(explosionSpawn);
			}
		}

		#endregion
	}
}
