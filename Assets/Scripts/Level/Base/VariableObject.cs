using UnityEngine;
using System.Collections.Generic;

namespace Konyisoft.Planet
{
	public class VariableObject : PooledObject
	{
		#region Fields

		public List<GameObject> variants = new List<GameObject>();
		public bool randomRotation90 = false;

		GameObject currentVariant;
		static readonly float[] rotations = { 0f, 90f, 180f, 270f };

		#endregion

		#region Properties

		protected LevelSegment ParentSegment
		{
			get { return GetComponentInParent<LevelSegment>(); }
		}

		#endregion

		#region Mono metods

		protected virtual void Awake()
		{
			if (variants.Count  > 0)
				DeactivateAllVariants();
		}
		protected virtual void Start()
		{
			// Empty at this time
		}

		#endregion

		#region Private methods

		void DeactivateAllVariants()
		{
			variants.ForEach(variant => SetObjectActive(variant, false));
		}

		void DeactivateCurrentVariant()
		{
			if (currentVariant != null)
				SetObjectActive(currentVariant, false);
		}

		void SetObjectActive(GameObject obj, bool active)
		{
			if (obj != null)
				obj.SetActive(active);
		}

		void RotateObject(GameObject obj) {
			if (obj != null)
				obj.transform.Rotate(0, rotations[Random.Range(0, rotations.Length)], 0, Space.Self);
		}

		#endregion

		#region Protected methods

		protected override void OnActivate()
		{
			base.OnActivate();
			// No variants
			if (variants.Count == 0)
			{
				if (randomRotation90)
					RotateObject(this.gameObject);
			}
			// Has some variants
			else
			{
				DeactivateCurrentVariant();
				// Activate a random variant object
				int randomIndex = Random.Range(0, variants.Count);
				SetObjectActive(variants[randomIndex], true);
				currentVariant = variants[randomIndex];
				if (randomRotation90)
					RotateObject(currentVariant);
			}
		}

		protected override void OnDeactivate()
		{
			base.OnDeactivate();
			if (variants.Count > 0)
				DeactivateCurrentVariant();
		}

		#endregion
	}
}
