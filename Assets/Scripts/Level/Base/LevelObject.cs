using UnityEngine;

namespace Konyisoft.Planet
{
	// Abstract class for all level objects which the player can interact
	public abstract class LevelObject : VariableObject
	{
		#region Fields

		public bool destroyOnImpact = true;

		#endregion

		#region Abstract methods

		protected abstract void OnCollideWithPlayer();

		#endregion

		#region Event handlers

		protected virtual void OnTriggerEnter(Collider other)
		{
			if (other.gameObject.CompareTag(Constants.Tags.Player))
			{
				// Object -> Spaceship direction
				Vector3 direction = (other.transform.position - transform.position).normalized;
				float forwardDot = Vector3.Dot(direction, Vector3.forward);
				// Hit from behind?
				if (forwardDot < 0.65f)
				{
					OnCollideWithPlayer();
					if (destroyOnImpact)
						gameObject.SetActive(false); // Do not destroy. Returns to the pool later.
				}
			}
		}

		#endregion
	}
}
