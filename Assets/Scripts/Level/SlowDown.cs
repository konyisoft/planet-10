namespace Konyisoft.Planet
{
	public class SlowDown : LevelObject
	{
		#region Protected methods

		protected override void OnCollideWithPlayer()
		{
			GameManager.Instance.SlowDown();
		}

		#endregion
	}
}
