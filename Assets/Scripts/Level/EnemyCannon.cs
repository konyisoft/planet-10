using UnityEngine;

namespace Konyisoft.Planet
{
	public class EnemyCannon : EnemyObject
	{
		#region Protected methods

		protected override void OnActivate()
		{
			base.OnActivate();
			// Different rotations of the object according to additional data value
			// NOTE: due to some GIMP bugs(?), during image editing, you should use different grey values than the real ones for alpha channel.
			switch (AdditionalData)
			{
				default:
				case 255:
					transform.rotation = Quaternion.identity;
					break;
				case 224:  // 241 in gimp
					transform.rotation = Quaternion.Euler(0, -45f, 0);
					break;
				case 192:  // 225
					transform.rotation = Quaternion.Euler(0, -90f, 0);
					break;
				case 161:  // 208  (NOTE: 160 can not be set in gimp, this is simply nonsense)
					transform.rotation = Quaternion.Euler(0, -135f, 0);
					break;
				case 128:  // 188
					transform.rotation = Quaternion.Euler(0, -180f, 0);
					break;
			}
		}

		#endregion
	}
}
