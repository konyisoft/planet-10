namespace Konyisoft.Planet
{
	public class Life : LevelObject
	{
		#region Protected methods

		protected override void OnCollideWithPlayer()
		{
			GameManager.Instance.GainOneLife();
		}

		#endregion
	}
}
