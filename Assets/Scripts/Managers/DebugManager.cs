﻿using System.Text;
using UnityEngine;

namespace Konyisoft.Planet
{
	public class DebugManager : Singleton<DebugManager>
	{
		#region Fields

		public bool showDebug;

		public float updateInterval = 0.5f;
		[Range(1, 100)]
		public int targetFrameRate = 60;

		float accum = 0f;
		int frames = 0;
		float timeLeft = 0f;
		float fps = 0f;
		float lastSample = 0f;

		#endregion

		#region Properties

		string FPSAsString
		{
			get { return fps.ToString("f2"); }
		}

		#endregion

		#region Mono methods

		void Awake()
		{
			timeLeft = updateInterval;
			lastSample = Time.realtimeSinceStartup;

			UIManager.Instance.SetDebugVisible(showDebug);
		}

		void Update()
		{
			if (showDebug)
			{
				// Measuring FPS
				frames++;
				float newSample = Time.realtimeSinceStartup;
				float deltaTime = newSample - lastSample;
				lastSample = newSample;

				timeLeft -= deltaTime;
				accum += 1.0f / deltaTime;

				if (timeLeft <= 0f)
				{
					fps = accum / frames;
					timeLeft = updateInterval;
					accum = 0f;
					frames = 0;
				}

				UIManager.Instance.DisplayDebug(GetDebugString());
			}
		}

		#endregion

		#region Private methods

		string GetDebugString()
		{
			Player player = PlayerManager.Instance.Player;
			StringBuilder sb = new StringBuilder();
			sb.AppendLine("FPS: " + FPSAsString);
			sb.AppendLine("Fuel: " + (player != null ? player.Fuel : 0));
			sb.AppendLine("Traveled: " + (player != null ? player.TraveledDistance : 0));
			sb.AppendLine("Current segment: " + LevelManager.Instance.CurrentSegmentIndex + " of " + LevelManager.Instance.SegmentCount);
			sb.AppendLine("Current row: " + LevelManager.Instance.CurrentRow + " of " + LevelManager.Instance.RowCount);
			if (player != null && player.IsCheating)
				sb.AppendLine("CHEAT MODE ON");
			return sb.ToString();
		}

		#endregion
	}
}
