﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Konyisoft.Planet
{
	public class GameManager : Singleton<GameManager>
	{
		#region Fields

		[Header("Global")]
		public int startLives = 3;
		public int maxLives = 9;
		public int scorePerTraveledUnit = 10;
		public Transform temporaryObject;

		[Header("Development")]
		public bool cheatMode = false;
		public int startSegmentIndex = 0;

		int lastTravelDistance = 0;

		#endregion

		#region Properties

		public int Lives
		{
			get; private set;
		}

		public int Score
		{
			get; private set;
		}

		#endregion

		#region Mono methods

		void Awake()
		{
			ResetValues();
		}

		void Start()
		{
			InitCheatMode();
			UpdateUI();
		}

		void Update()
		{
			Player player = PlayerManager.Instance.Player;
			// Score for traveled distance
			if (player.IsFlying)
			{
				int travelDistance = (int)player.TraveledDistance;
				if (travelDistance > lastTravelDistance) {
					lastTravelDistance = travelDistance;
					IncreaseScore(scorePerTraveledUnit);
				}
				UpdateScoreUI();
				UpdateFuelUI();
			}

			// Continue game or restart
			if (player.IsDead && (Input.GetKeyUp(KeyCode.Return) || Input.GetKeyUp(KeyCode.KeypadEnter)))
			{
				if (Lives > 0)
				{
					RevivePlayer();
				}
				else
				{
					RestartGame();
				}
			}

			// Cheat mode on/off
			if (Input.GetKeyUp(KeyCode.C))
			{
				if (player.IsCheating)
				{
					player.DisableCheat();
				}
				else
				{
					player.EnableCheat();
				}
			}
		}

		#endregion

		#region Public methods

		public void UpdateScoreUI()
		{
			UIManager.Instance.DisplayScore(Score);
		}

		public void UpdateLivesUI()
		{
			UIManager.Instance.DisplayLives(Lives);
		}

		public void UpdateFuelUI()
		{
			UIManager.Instance.DisplayFuel(PlayerManager.Instance.Player.FuelNormalized);
		}

		public void UpdateUI()
		{
			UpdateScoreUI();
			UpdateLivesUI();
			UpdateFuelUI();
		}

		public void MaximizeFuel()
		{
			PlayerManager.Instance.Player.MaximizeFuel();
			UIManager.Instance.DisplayMessage(Constants.Texts.Message.MaximizeFuel);
		}

		public void OutOfFuel()
		{
			PlayerManager.Instance.Player.Die();
			LoseOneLife();
			if (Lives == 0)
			{
				GameOver();
			}
			else
			{
				UIManager.Instance.DisplayMessage(Constants.Texts.Message.OutOfFuel, false);
			}
		}

		public void SpeedUp()
		{
			PlayerManager.Instance.Player.SpeedUp();
			UIManager.Instance.DisplayMessage(Constants.Texts.Message.SpeedUp);
		}

		public void SlowDown()
		{
			PlayerManager.Instance.Player.SlowDown();
			UIManager.Instance.DisplayMessage(Constants.Texts.Message.SlowDown);
		}

		public void Bonus(int amount)
		{
			IncreaseScore(amount);
			UIManager.Instance.DisplayMessage(string.Format(Constants.Texts.Message.Bonus, amount));
		}

		// Impact with obstacle, enemy or enemy bullet
		public void Impact()
		{
			PlayerManager.Instance.Player.Die();
			LoseOneLife();
			if (Lives == 0)
			{
				GameOver();
			}
			else
			{
				UIManager.Instance.DisplayMessage(Constants.Texts.Message.Failed, false);
			}
		}

		public void GainOneLife()
		{
			if (Lives < maxLives)
			{
				Lives++;
				UpdateLivesUI();
				UIManager.Instance.DisplayMessage(Constants.Texts.Message.GainOneLife);
			}
			else
			{
				UIManager.Instance.DisplayMessage(Constants.Texts.Message.LivesOnMaximum);
			}
		}

		public void LoseOneLife()
		{
			if (Lives > 0)
			{
				Lives--;
				UpdateLivesUI();
			}
		}

		public void IncreaseScore(int amount) {
			Score += amount;
		}

		public void RevivePlayer()
		{
			if (Lives > 0)
			{
				UIManager.Instance.ClearMessage();
				PlayerManager.Instance.Player.Revive();
			}
		}

		public void GameOver()
		{
			UIManager.Instance.DisplayMessage(Constants.Texts.Message.GameOver, false);
		}

		public void Won()
		{
			UIManager.Instance.DisplayMessage(Constants.Texts.Message.Won, false);
		}

		public void RestartGame()
		{
			// Simply reload the scene. This is the easiest solution.
			SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex);
		}

		#endregion

		#region Private methods

		void ResetValues()
		{
			Score = 0;
			Lives = startLives;
			lastTravelDistance = 0;
		}

		void InitCheatMode()
		{
			// For development only: cheat mode
			if (cheatMode)
				PlayerManager.Instance.Player.EnableCheat();
		}

		#endregion
	}
}
