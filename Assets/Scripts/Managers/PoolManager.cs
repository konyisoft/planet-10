﻿using UnityEngine;

namespace Konyisoft.Planet
{
	public class PoolManager : Singleton<PoolManager>
	{
		#region Fields

		public Transform poolsParent;

		#endregion

		#region Mono methods

		void Awake()
		{
			if (poolsParent == null)
				poolsParent = transform; // this
		}

		#endregion

		#region Puiblic method

		public void ReturnToPool(Component component)
		{
			// Returns any object to the pool (which has PooledObject component attached)
			PooledObject pooledObject = component.GetComponent<PooledObject>();
			if (pooledObject != null)
				pooledObject.ReturnToPool();
		}

		#endregion
	}
}
