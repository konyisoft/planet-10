﻿using UnityEngine;

namespace Konyisoft.Planet
{
	public class EnemyManager : Singleton<EnemyManager>
	{
		#region Fields

		public Transform enemiesParent;
		[Header("Defaults for all enemies")]
		public Bullet bulletPrefab;
		public Explosion explosionPrefab;
		public float operatingDistance = 7f;
		public float terminateDistance = -7f;

		#endregion

		#region Mono methods

		void Awake()
		{
			if (enemiesParent == null)
				enemiesParent = transform; // this
		}

		#endregion
	}
}
