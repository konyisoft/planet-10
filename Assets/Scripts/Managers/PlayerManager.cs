﻿using UnityEngine;

namespace Konyisoft.Planet
{
	public class PlayerManager : Singleton<PlayerManager>
	{
		#region Fields

		public Player player;
		public Camera mainCamera;
		public Bullet bulletPrefab;
		public Explosion explosionPrefab;
		public float cameraFollowSpeed = 1f;
		Vector3 cameraStartPosition;

		#endregion

		#region Properties

		public Player Player
		{
			get { return player; }
		}

		#endregion

		#region Mono methods

		void Awake()
		{
			if (player == null)
			{
				Debug.LogError("No player attached.");
				return;
			}

			if (mainCamera == null)
			{
				Debug.LogError("No main camera attached.");
				return;
			}

			cameraStartPosition = mainCamera.transform.position;
			LevelManager.Instance.onUpdateEnded += new LevelManager.OnUpdateEnded(this.OnUpdateEnded);
		}

		void Start()
		{
			FollowPlayerWithCamera(true);
			player.Fly();
		}

		void Update()
		{
			if (player == null)
				return;

			// Keyboard input and camera follow
			if (player.IsFlying)
			{
				float horizontal = 0;
				float vertical = 0;

				// Left
				if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
				{
					horizontal = -1f;
				}
				// Right
				else if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
				{
					horizontal = 1f;
				}

				// Forward
				if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))
				{
					vertical = 1f;
				}
				// Back
				else if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
				{
					vertical = -1f;
				}

				// Shooting
				if (Input.GetKeyDown(KeyCode.Space))
				{
					player.Shoot();
				}
				else if (Input.GetKeyUp(KeyCode.Space))
				{
					player.DontShoot();
				}

				player.Move(new Vector2(horizontal, vertical));

			}

			if (mainCamera != null)
				FollowPlayerWithCamera();
		}

		#endregion

		#region Private methods

		void FollowPlayerWithCamera(bool immediately = false)
		{
			Vector3 cameraPosition = mainCamera.transform.position;
			Vector3 followPosition = player.transform.position + cameraStartPosition;
			float speed = cameraFollowSpeed * (player.IsDead ? 0.25f : 1f);
			// Lerp horizontal position
			cameraPosition.x = immediately ?
				followPosition.x :
				Mathf.Lerp(cameraPosition.x, followPosition.x, speed * Time.deltaTime);
			mainCamera.transform.position = cameraPosition;
		}

		#endregion

		#region Event handlers

		void OnUpdateEnded()
		{
			player.Stop();
			Debug.Log("Level ended.");
		}

		#endregion

	}
}
