﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Konyisoft.Planet
{
	public class LevelManager : Singleton<LevelManager>
	{
		#region Delegates

		public delegate void OnUpdateEnded();

		#endregion

		#region Fields

		[Header("Settings")]
		public Transform segmentsParent;
		public LevelSegment segmentPrefab;
		public int rowsPerSegment = 5;
		public int startRow = 0;
		public int displayingSegments = 3;
		public float objectSize = 1f;

		[Header("Objects")]
		public Texture2D objectTexture;
		public Vector3 objectOffset;

		[Header("Ground")]
		public Texture2D groundTexture;
		public Vector3 groundOffset;

		public event OnUpdateEnded onUpdateEnded; // called when no more segments to update

		Dictionary<Color32, PooledObject> objectPrefabs = new Dictionary<Color32, PooledObject>();
		Dictionary<Color32, PooledObject> groundPrefabs = new Dictionary<Color32, PooledObject>();
		Color32[] objectPixels;
		Color32[] groundPixels;
		List<LevelSegment> segments;
		float segmentHeight;
		int segmentCount;   // maximum number of segments
		int lastSegmentIndex;
		int additionalRows;
		int startSegmentIndex;

		#endregion

		#region Properties

		public int SegmentCount
		{
			get { return segmentCount; }
		}

		public int CurrentSegmentIndex
		{
			get
			{
				 return (int)(CurrentRow / segmentHeight);
			}
		}

		public int RowCount
		{
			get { return objectTexture != null ? objectTexture.height : 0; }
		}

		public int CurrentRow
		{
			get
			{
				return (int)(PlayerManager.Instance.Player.TraveledDistance * objectSize) + additionalRows;
			}
		}

		public Color32[] ObjectPixels
		{
			get { return objectPixels; }
		}

		public Color32[] GroundPixels
		{
			get { return groundPixels; }
		}

		#endregion

		#region Mono methods

		void Awake()
		{
			if (objectTexture == null)
			{
				Debug.LogError("No object texture attached.");
				return;
			}

			if (groundTexture == null)
			{
				Debug.LogError("No ground texture attached.");
				return;
			}

			if (segmentPrefab == null)
			{
				Debug.LogError("No segment prefab attached.");
				return;
			}

			if (startRow < 0 || startRow > rowsPerSegment)
			{
				Debug.LogError("Value of startRow must be between 0 and rowsPerSegment.");
				return;
			}

			if (segmentsParent == null)
				segmentsParent = transform;

			Initialize();
		}

		void Start()
		{
			CreateSegments();
			SetStartPosition();
		}

		void Update()
		{
			Player player = PlayerManager.Instance.Player; // shortcut

			if (!player.IsFlying)
				return;

			int index = CurrentSegmentIndex;
			if (index <= segmentCount)
			{
				// Move all segments towards the player (opposite direction of player forward)
				Vector3 translation = -player.Velocity * Time.deltaTime;
				TranslateSegments(translation);

				// Perform update if segment changed (after 'startRow' count of rows left behind)
				if (index > lastSegmentIndex && CurrentRow % rowsPerSegment >= startRow)
				{
					UpdateSegments();
					lastSegmentIndex = index;
				}
			}
			else
			{
				if (onUpdateEnded != null)
					onUpdateEnded();
			}
		}

		#endregion

		#region Private methods

		void Initialize()
		{
			// Get the pixels of textures. Arrays turned upside down.
			objectPixels = objectTexture.GetPixels32();
			Array.Reverse(objectPixels);
			groundPixels = groundTexture.GetPixels32();
			Array.Reverse(groundPixels);

			// Create color/prefab dictionaries from constants data
			LoadPrefabs(Constants.ResourceData.Objects, objectPrefabs);
			LoadPrefabs(Constants.ResourceData.Grounds, groundPrefabs);

			// List of segment objects
			segments = new List<LevelSegment>();

			// Calculate total number of segments
			segmentCount = (int)(objectTexture.height / rowsPerSegment);

			// Other segment values
			segmentHeight = rowsPerSegment * objectSize;
			startSegmentIndex = Mathf.Clamp(GameManager.Instance.startSegmentIndex, 0, segmentCount - 1);
			lastSegmentIndex = startSegmentIndex;
			// Starting segment in rows + the starting row inside that segment
			additionalRows = (int)(startSegmentIndex * segmentHeight) + startRow;
		}

		void CreateSegments()
		{
			if (displayingSegments <= segmentCount)
			{
				for (int i = 0; i < displayingSegments; i++)
				{
					LevelSegment segment = Instantiate(segmentPrefab).GetComponent<LevelSegment>();
					LevelSegment alignTo = i == 0 ? null : segments[i - 1];
					segment.Create(i + startSegmentIndex, objectTexture.width, rowsPerSegment, alignTo);
					segment.transform.SetParent(segmentsParent, false);
					segment.gameObject.name = string.Format("Segment {0:00}", i);
					segments.Add(segment);
				}
			}
		}

		void TranslateSegments(Vector3 translation)
		{
			for (int i = 0; i < segments.Count; i++)
			{
				segments[i].transform.Translate(translation, Space.World);
			}
		}

		void UpdateSegments()
		{
			int updatingSegmentIndex = lastSegmentIndex + displayingSegments;

			if (updatingSegmentIndex < segmentCount)
			{
				// Remove first segment in the list and create a new one
				LevelSegment segment = segments[0];
				LevelSegment alignTo = segments[segments.Count - 1];
				segment.Clear();
				segment.Create(updatingSegmentIndex, objectTexture.width, rowsPerSegment, alignTo);
				segments.RemoveAt(0); // remove first element
				segments.Add(segment); // add newly created segment to the end
			}
			else if (segments.Count > 0)
			{
				// Remove first element, no creation here
				segments[0].Clear();
				segments.RemoveAt(0);
			}
			// NOTE: This should never be executed
			else
			{
				Debug.Log("No segments to update.");
			}
		}

		void ClearSegments()
		{
			for (int i = 0; i < segments.Count; i++)
			{
				segments[i].Clear();
			}
			segments.Clear();
			segmentCount = 0;
			lastSegmentIndex = 0;
		}

		void SetStartPosition()
		{
			if (startRow > 0) {
				Vector3 translation = new Vector3(0, 0, -startRow * objectSize);
				TranslateSegments(translation);
			}
		}

		void LoadPrefabs(Dictionary<string, string> data, Dictionary<Color32, PooledObject> prefabs)
		{
			foreach (KeyValuePair<string, string> kvp in data)
			{
				Color color;
				ColorUtility.TryParseHtmlString(kvp.Key, out color);
				Color32 color32 = color;
				color32.a = 255;
				PooledObject pooledObject = Resources.Load<PooledObject>(kvp.Value);
				if (pooledObject != null)
					prefabs.Add(color32, pooledObject);
			}
		}

		#endregion

		#region Public methods

		public PooledObject GetObjectPrefab(Color32 c)
		{
			c.a = 255;  // key colors' alpha is always 255
			return objectPrefabs.ContainsKey(c) ?  objectPrefabs[c] : null;
		}

		public PooledObject GetGroundPrefab(Color32 c)
		{
			c.a = 255;
			return groundPrefabs.ContainsKey(c) ? groundPrefabs[c] : null;
		}

		#endregion
	}
}
