﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Konyisoft.Planet
{
	public class UIManager : Singleton<UIManager>
	{
		#region Fields

		public Text scoreText;
		public Text livesText;
		public Text debugText;
		public Image fuelImage;
		public Gradient fuelGradient;
		public Text messageText;
		public float messageDuration = 4;

		float origFuelImageWidth;

		const string MessageCoroutine = "CO_Message";

		#endregion

		#region Mono methods

		void Awake()
		{
			// Keep original width
			if (fuelImage != null)
				origFuelImageWidth = fuelImage.rectTransform.rect.width;

			ClearMessage();
		}

		#endregion

		#region Public methods

		public void DisplayScore(int score)
		{
			if (scoreText != null)
				scoreText.text =  string.Format(Constants.Texts.Score, score);
		}

		public void DisplayLives(int lives)
		{
			if (livesText != null)
				livesText.text = string.Format(Constants.Texts.Lives, lives);
		}

		public void DisplayDebug(string text)
		{
			if (debugText != null)
				debugText.text = text;
		}

		public void DisplayMessage(string text, bool autohide = true)
		{
			StopCoroutine(MessageCoroutine);
			if (messageText != null)
			{
				if (autohide)
				{
					StartCoroutine(MessageCoroutine, text);
				}
				else
				{
					messageText.text = text;
				}
			}
		}

		public void ClearMessage()
		{
			StopCoroutine(MessageCoroutine);
			DisplayMessage("");
		}

		public void SetDebugVisible(bool visible)
		{
			if (debugText != null)
				debugText.gameObject.SetActive(visible);
		}

		public void DisplayFuel(float normalizedValue)
		{
			if (fuelImage != null)
			{
				normalizedValue = Mathf.Clamp(normalizedValue, 0f, 1f);
				float imageWidth = origFuelImageWidth * normalizedValue;
				fuelImage.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, imageWidth);
				fuelImage.color = fuelGradient.Evaluate(normalizedValue);
			}
		}

		#endregion

		#region Private methods

		IEnumerator CO_Message(string text)
		{
			messageText.text = text;
			float startTime = Time.time;
			while (Time.time - startTime < messageDuration)
			{
				yield return null;
			}
			messageText.text = "";
		}

		#endregion
	}
}
